output "eks_cluster_endpoint" {
  value = aws_eks_cluster.yavinenana-cluster.endpoint
}

output "eks_cluster_certificat_authority" {
  value = aws_eks_cluster.yavinenana-cluster.certificate_authority 
}
