resource "aws_eks_node_group" "node" {
  cluster_name    = aws_eks_cluster.yavinenana-cluster.name
  node_group_name = "nodes-${aws_eks_cluster.yavinenana-cluster.name}"
  node_role_arn   = aws_iam_role.eks_nodes.arn
//  subnet_ids      = ["subnet-4d0d3324", "subnet-b3a8f9c8"] 
# the subnets needs to apply public ip to nodes ec2, else it can be failed
  subnet_ids = flatten([var.subnets])

  scaling_config {
    desired_size = 3
//    max_size     = 1
//    min_size     = 1
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
  ]
}
