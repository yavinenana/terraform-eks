# tfmodules/ec2/main.tf
remote_state { 
//terraform {
//  required_version = ">= 0.12.28"
//  backend "s3" {
  backend = "s3" 
  config = { 
    bucket         = "bucket-s3-name"
    key            = "terraform-eks.tfstate"
    region         = "us-east-1"
    encrypt        = false
    dynamodb_table = "terraform_locks_eks_table"
    profile        = "awsprofile"
  } 
}
