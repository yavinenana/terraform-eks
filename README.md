REPOSITORIO PÚBLICO - TERRAFORM EKS
# Version terraform 
  vs terragrunt (but it repo dont support terragrunt apply)
	terraform-v0.13.5     VS    terragrunt-v0.25.0
	version terraform to download: terraform_0.13.5_linux_amd64.zip
# Terraform v0.13.5

# vpc-produseast1
 - run commands: 
# to download plugin aWS provider to terraform
 . terraform init
# planing stack
 . terraform plan
# deploy stack
 . terraform apply 
# destroy stack - BE CAREFUL TO PRODUCTION
 . terraform destroy 
#  to get state and output 
. terraform state list
# to update new modules
. terraform get -update=true

# ENVIROMENTS STATES
# PUT BEFORE TF_LOG=DEBUG to enable mode debug
. terraform workspace new    $WORKSPACE
. terraform workspace list
. terraform workspace select $WORKSPACE
. terraform plan    -var-file tfmodules/ec2/terraform.tfvars
. terraform apply   -var-file tfmodules/ec2/terraform.tfvars
. terraform destroy -var-file tfmodules/ec2/terraform.tfvars

# ###########################
- Run main.tf that it has modules to import code from tfmodules/ec2/ 
- This it will call resources of ec2 from tfmodules/ec2
- it will create 2 ec2 for resource and 1 sec group

tfmodules/ec2/terraform.tfvars --> it's like constructor from code (almacena parametros , pero igual puedes cambiarlas al instanciarlas
tfmodules/ec2/variables.tf     --> define variables to use in declarator resource from tfmodules/ec2/main.tf

[FEAT] EC2 - SEC GROUP - IAM ROLE - POLICY -- to server ec2 

# policy 
. s3 putobject, getobject, getobjectacl,putobjectacl - in any buckets. (1)
# role
. iam role, iam policy attachment, iam instance profile (3)
# sec group
. sg 80,8069,22	(1)
# ec2
. ec2 to (1)
