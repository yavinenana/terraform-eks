// https://github.com/terraform-aws-modules/terraform-aws-vpc/issues/271
resource "aws_eks_cluster" "yavinenana-cluster" {
  name     = var.name
  role_arn = aws_iam_role.eks_role.arn

  vpc_config {
//    subnet_ids = [aws_subnet.example1.id, aws_subnet.example2.id]
//    subnet_ids              = [module.vpc.public_subnets, module.vpc.private_subnets]
//    subnet_ids = ["${aws_subnet.priv_subnet.*.id}"]
    subnet_ids = flatten([var.subnets])
//    security_group_ids      = [aws_security_group.cluster.id]
    endpoint_private_access = "true"
    endpoint_public_access  = "true"
}

  # Ensure that IAM Role permissions are created before and deleted after EKS Cluster handling.
  # Otherwise, EKS will not be able to properly delete EKS managed EC2 infrastructure such as Security Groups.
  depends_on = [
    aws_iam_role_policy_attachment.attach-p1,
    aws_iam_role_policy_attachment.attach-p2
  ]
}

output "endpoint" {
  value = aws_eks_cluster.yavinenana-cluster.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.yavinenana-cluster.certificate_authority[0].data
}
