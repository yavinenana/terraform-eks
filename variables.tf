variable "miregion" {
  type = string
  description = "region aws"
}
variable "miprofile" {}

variable "name" {}
variable "nodes" {
  type = number
}

variable "subnets" {
  type = list(string)
  description = "list of subnets publics to deploy nodes"
}
variable "vpc_id" {
  type = string
}

variable "enviroment" {}
