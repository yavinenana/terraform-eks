// RESOURCE IAM ROLE
resource "aws_iam_role" "eks_role" {
  name               = "eks-role-${var.name}"
  assume_role_policy = file("assumerolepolicy.json")
  description = "role eks full - * regions"
}

// RESOURCE IAM POLICY
resource "aws_iam_policy" "AmazonEKSClusterPolicy" {
  name        = "AmazonEKSClusterPolicy-eks-${var.name}"
  description = " This policy provides Kubernetes the permissions it requires to manage resources on your behalf. Kubernetes requires Ec2:CreateTags permissions to place identifying information on EC2 resources including but not limited to Instances, Security Groups, and Elastic Network Interfaces."
  policy      = file("AmazonEKSClusterPolicy.json")
}
resource "aws_iam_policy" "AmazonEKSServiceRolePolicy" {
  name        = "AmazonEKSServiceRolePolicy-eks-${var.name}"
  description = " A Service-Linked Role required for Amazon EKS to call AWS services on your behalf. "
  policy      = file("AmazonEKSServiceRolePolicy.json")
}

// RESOURCE IAM POLICY ATTACHMENT
resource "aws_iam_role_policy_attachment" "attach-p1" {
//  name        = "attachment-${var.name}"
//  role        = aws_iam_role.eks_role.name
//  policy_arn  = aws_iam_policy.AmazonEKSClusterPolicy.arn
  role       = aws_iam_role.eks_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}
resource "aws_iam_role_policy_attachment" "attach-p2" {
  role        = aws_iam_role.eks_role.name
  policy_arn  = aws_iam_policy.AmazonEKSServiceRolePolicy.arn
}

// RESOURCE IAM INSTANCE PROFILE
resource "aws_iam_instance_profile" "miprofile" {
  name        = "test_profile-${var.name}" 
  role       = aws_iam_role.eks_role.name
}

terraform {
  backend "s3" {}
}
